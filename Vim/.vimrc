syntax enable
set background=dark
colorscheme solarized8_dark_low
set t_Co=256
set number
set autoread
set cpoptions+=n
syntax on
set splitright

filetype plugin indent on
execute pathogen#infect()

let g:airline_theme='wombat'

" nerdtree 
map <C-n> :NERDTreeToggle<CR>

autocmd bufenter * if (winnr("$") == 1 && exists("b:NERDTree") && b:NERDTree.isTabTree()) | q | endif

" tagbar
nmap <F8> :TagbarToggle<CR>


